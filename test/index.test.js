const assert = require('assert');
const fastify = require('fastify');
const plugin = require('..');

describe('session', () => {
  describe('#read and save', () => {
    let app = null;

    before(() => {
      app = fastify();
      app.register(require('fastify-cookie'), {
        secret: ['23a79fa38d2578d6'],
      }); // cookie
      app.register(plugin, {
        renew: true,
      });
      app.get('/get', (req, reply) => {
        let session = req.session;
        reply.send(JSON.stringify(session));
      });
      app.get('/save', (req, reply) => {
        req.session = { login: true };
        reply.send('SUCCESS');
      });
    });

    after(() => {
      app.close();
      app = null;
    });

    it('read', () => {
      app.inject(
        {
          method: 'get',
          url: '/get',
        },
        (err, res) => {
          assert.ifError(err);
          assert.strictEqual(res.statusCode, 200);
          assert.strictEqual(res.body, '{}');
        },
      );
    });

    it.only('save_read', function (done) {
      app.inject(
        {
          method: 'get',
          url: '/save',
        },
        (err, res) => {
          assert.ifError(err);
          assert.strictEqual(res.statusCode, 200);
          assert.strictEqual(res.body, 'SUCCESS');
          assert.strictEqual(res.cookies[0].name, 'fastify_sess');
          done();
        },
      );
    });
  });
});
