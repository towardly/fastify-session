# fastify-pithy-session

[Fastify](https://www.fastify.io/)插件用于读取和设置 `session`。

## 原理
1. 在 `fastify` 的 `onRequest` 钩子里面解析 `session` 同时计算 `session` 值的 `hash` 保存到 `fastify.prevSessionHash` 里面；如果有 `renew = true`，则在 `fastify.prevSessionExpire` 保存过期信息。
2. 在 `fastify` 的 `onSend` 钩子里面判断是否需要重新保存，依据如下：
    * 进行过 `session` 更改, 判断当前 `session` 的 `hash` 是否和 `fastify.prevSessionHash` 一致。
    * `rolling` 是否为 `true`
    * `renew = true` 并且快要过期。

## 依赖

当前插件依赖 [fastify-cookie](https://github.com/fastify/fastify-cookie)

## 安装

```Bash
npm i fastify-cookie fastify-pithy-session
```

## 配置

引入并注册到 `fastify` 插件。

```js
const fastify = require('fastify')();
app.register(require('fastify-cookie'), {
  // 如果 session 配置了 signed 则需要添加这个参数进行签名。
  secret: ['23a79fa38d2578d6'],
});
fastify.register(require('@tenny/fastify-session'), {
  renew: true,
  rolling: false,
  key: 'fastify_sess',
  cookie: {
    maxAge: 86400, // 到期时间, 一天
    httpOnly: true,
    signed: true,
  },
});

fastify.listen(3000);
```

## 配置项

1. `key` - { `String` }: `cookie key`，默认为：`'fastify_sess'`
2. `rolling` - { `Boolean` }: 强制每一次响应的时候都进行一次数据保存，默认为：`false`
3. `renew` - { `Boolean` }: 当 session 快过期的时候，保存数据，重新刷新有效期，默认为：`false`
4. `cookie` - { `Object` }：[fastify-cookie Sending options](https://github.com/fastify/fastify-cookie)配置项，默认：`{ maxAge: 86400, httpOnly: true, signed: true }`

> 如果同时配置了 `rolling` 和 `renew` 则 `rolling` 的优先级高于 `renew`，就是说如果配置了 `rolling` 则不需要配置 `renew`。

## 使用
```JavaScript
// 1. 获取 session
let session = req.session;

// 2. 设置 session
req.session = { login: true };
// 或者
req.session.login = true;
```

## fastify 版本 - `3.x`
