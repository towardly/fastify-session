/**
 * Copyright (c) [2021] [tenny]
   [fastify-pithy-session] is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2. 
   You may obtain a copy of Mulan PSL v2 at:
            http://license.coscl.org.cn/MulanPSL2 
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
   See the Mulan PSL v2 for more details. 
 */
import { FastifyPlugin } from 'fastify';

declare module 'fastify' {
  interface FastifyRequest {
    prevSessionHash: string;
    session: unknown;
    prevSessionExpire?: {
      expire: number;
      maxAge: number;
    };
  }
}

export interface FastifySessionOptions {
  key?: string;
  rolling?: boolean;
  renew?: boolean;
  cookie?: {
    domain?: string;
    encode?(val: string): string;
    expires?: Date;
    httpOnly?: boolean;
    maxAge?: number;
    path?: string;
    sameSite?: boolean | 'lax' | 'strict' | 'none';
    secure?: boolean;
    signed?: boolean;
  };
}

declare const fastifySession: FastifyPlugin<FastifySessionOptions>;
export default fastifySession;
